#ifndef GrpImage_H
#define GrpImage_H
#ifdef _MSC_VER
#include <windows.h>
#else
#include <experimental/filesystem>
#endif
#include <TinyEXIF.h>
#include <vector>	//std::vector
#include <string>	//std::string
#include <map>		//std::map
#include <regex>	//std::regex, std::regex_match
#include <iostream> // std::cout
#include <fstream>  // std::ifstream

class GrpImage
{
private:
	class EXIFStreamFile : public TinyEXIF::EXIFStream {
	public:
		explicit EXIFStreamFile(const char* fileName)
			: file(fileName, std::ifstream::in | std::ifstream::binary) {}
		bool IsValid() const override {
			return file.is_open();
		}
		const uint8_t* GetBuffer(unsigned desiredLength) override {
			buffer.resize(desiredLength);
			if (!file.read((char*)buffer.data(), desiredLength))
				return NULL;
			return buffer.data();
		}
		bool SkipBuffer(unsigned desiredLength) override {
			return (bool)file.seekg(desiredLength, std::ios::cur);
		}
	private:
		std::ifstream file;
		std::vector<uint8_t> buffer;
	};

	std::string dir;
	std::vector<std::string> dir_list;
	std::map<double, std::string> Reso_map;
	std::map<std::string, size_t> Brand_map;
	std::map<std::string, size_t> Model_map;
	int focal_arr[4];
	void read_directory(const std::string& name, std::vector<std::string>& v);
	void img_stats();
public:
	GrpImage(std::string str);
	std::vector<std::string> GetList();
	std::string Print_stats();
};


#endif
