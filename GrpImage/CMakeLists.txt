cmake_minimum_required(VERSION 3.1)

project(GrpImage)
include(GNUInstallDirs)

find_package(tinyxml2 REQUIRED)
find_package(TinyEXIF REQUIRED)


set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

#build as static library
add_library(GrpImage STATIC GrpImage.cpp GrpImage.h)
target_link_libraries(GrpImage tinyxml2)
target_link_libraries(GrpImage TinyEXIF )

if(DEFINED CMAKE_VERSION AND NOT "${CMAKE_VERSION}" VERSION_LESS "2.8.11")
	target_include_directories(GrpImage PUBLIC 
							$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
							$<INSTALL_INTERFACE:${CMAKE_INSTALL_PREFIX}/include>)
else()
		include_directories(${PROJECT_SOURCE_DIR})
endif()

export(TARGETS GrpImage
			FILE ${CMAKE_BINARY_DIR}/${CMAKE_PROJECT_NAME}Targets.cmake)

	install(TARGETS GrpImage
			EXPORT ${CMAKE_PROJECT_NAME}Targets
			RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
			LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
			ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})

SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
SET(BUILD_SHARED_LIBS OFF)
#SET(CMAKE_EXE_LINKER_FLAGS "-static")

add_executable(GrpImagedemo Main.cpp)
add_dependencies(GrpImagedemo GrpImage)
target_link_libraries(GrpImagedemo GrpImage)
target_link_libraries(${PROJECT_NAME} stdc++fs)
target_link_libraries(${PROJECT_NAME} curl)
target_compile_definitions(GrpImagedemo PRIVATE GrpImage_IMPORT)

install(FILES GrpImage.h DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

foreach(p LIB INCLUDE)
	set(var CMAKE_INSTALL_${p}DIR)
	if(NOT IS_ABSOLUTE "${${var}}")
		set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
	endif()
endforeach()

file(WRITE
	${CMAKE_BINARY_DIR}/${CMAKE_PROJECT_NAME}Config.cmake
	"include(\${CMAKE_CURRENT_LIST_DIR}/${CMAKE_PROJECT_NAME}Targets.cmake)\n")

install(FILES
		${CMAKE_BINARY_DIR}/${CMAKE_PROJECT_NAME}Config.cmake
		DESTINATION lib/cmake/${CMAKE_PROJECT_NAME})
		
install(EXPORT ${CMAKE_PROJECT_NAME}Targets
		DESTINATION ${PROJECT_SOURCE_DIR})
