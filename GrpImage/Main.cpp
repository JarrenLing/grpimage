#include "GrpImage.h"
#include <stdio.h>
#include <vector>
#include <string>
#include <iostream> // std::cout
#include <ctime>
#include <stdlib.h> // defines putenv in POSIX
#include <experimental/filesystem>
#include <curl/curl.h>

#define print_dir 0

/*****
Helper functions for Curl call to access and download from url
*****/
size_t AppendDataToStringCurlCallback(void *ptr, size_t size, size_t nmemb, void *vstring)
{
	std::string * pstring = (std::string*)vstring;
	pstring->append((char*)ptr, size * nmemb);
	return size * nmemb;
}
 
 size_t callbackfunction(void *ptr, size_t size, size_t nmemb, void* userdata)
{
		FILE* stream = (FILE*)userdata;
		if (!stream)
		{
				printf("!!! No stream\n");
				return 0;
		}

		size_t written = fwrite((FILE*)ptr, size, nmemb, stream);
		return written;
}

std::string DownloadUrlAsString(const std::string & url)
{
	std::string body;
	
	CURL *curl_handle;
	curl_global_init(CURL_GLOBAL_ALL);
	curl_handle = curl_easy_init();
	curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, AppendDataToStringCurlCallback);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &body);
	curl_easy_perform(curl_handle); 
	curl_easy_cleanup(curl_handle);
	
	return body;
}
 
bool download_jpeg(const char* url, std::string PhotoID)
{
		std::string filename = "flickr/" + PhotoID + ".jpg";
		FILE* fp = fopen(filename.c_str(), "wb");
		if (!fp)
		{
				printf("!!! Failed to create file on the disk\n");
				return false;
		}

		CURL* curlCtx = curl_easy_init();
		curl_easy_setopt(curlCtx, CURLOPT_URL, url);
		curl_easy_setopt(curlCtx, CURLOPT_WRITEDATA, fp);
		curl_easy_setopt(curlCtx, CURLOPT_WRITEFUNCTION, callbackfunction);
		curl_easy_setopt(curlCtx, CURLOPT_FOLLOWLOCATION, 1);

		CURLcode rc = curl_easy_perform(curlCtx);
		if (rc)
		{
				printf("!!! Failed to download: %s\n", url);
				return false;
		}

		long res_code = 0;
		curl_easy_getinfo(curlCtx, CURLINFO_RESPONSE_CODE, &res_code);
		if (!((res_code == 200 || res_code == 201) && rc != CURLE_ABORTED_BY_CALLBACK))
		{
				printf("!!! Response code: %ld\n", res_code);
				return false;
		}

		curl_easy_cleanup(curlCtx);

		fclose(fp);

		return true;
}

/*****
main function
*****/
int main(int argc, const char** argv)
{
	CURL *curl;
	CURLcode res;
	std::string data;
	
	curl = curl_easy_init();
	if(curl) 
	{
		std::string web_url;
		if (argc != 2) 
			web_url = "https://www.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=8902d5bed5f6beb098f8d3fb5c17b8c8&per_page=500&format=rest";
		else
			web_url = argv[1];

		curl_easy_setopt(curl, CURLOPT_URL, web_url.c_str());
		/* example.com is redirected, so we tell libcurl to follow redirection */ 
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		/* Perform the request, res will get the return code */ 
		data = DownloadUrlAsString(web_url);
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
							curl_easy_strerror(res));
		
		/* always cleanup */ 
		curl_easy_cleanup(curl);
	}
	
	size_t finder = 0;
	size_t ender = 0;
	std::vector<std::string> PhotoID;
	std::vector<std::string> SecretID;
	std::vector<std::string> Photo_url;

	//find first photoID
	finder = data.find("photo id=");
	while(finder!=std::string::npos)
	{
		//find PhotoID
		finder += 10;
		ender = data.find("\"",finder);
		std::string s_PhotoID = data.substr(finder,ender-finder);
		PhotoID.push_back(s_PhotoID);
		
		//find next PhotoID
		finder = data.find("photo id=",finder);
	}
	
	for(auto i : PhotoID)
	{
		curl = curl_easy_init();
		if(curl) 
		{
			std::string web_url = "https://www.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=8902d5bed5f6beb098f8d3fb5c17b8c8&photo_id=";
			web_url+= i +"&format=rest";
	
			curl_easy_setopt(curl, CURLOPT_URL, web_url.c_str());
			/* example.com is redirected, so we tell libcurl to follow redirection */ 
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
			/* Perform the request, res will get the return code */ 
			data = DownloadUrlAsString(web_url);
			res = curl_easy_perform(curl);
			/* Check for errors */ 
			if(res != CURLE_OK)
				fprintf(stderr, "curl_easy_perform() failed: %s\n",
								curl_easy_strerror(res));
			
			/* always cleanup */ 
			curl_easy_cleanup(curl);
			
			finder = data.rfind("source=");
			finder += 8;
			ender = data.find("\"",finder);
			std::cout<<data.size()<<" "<<finder<<" "<<ender<<std::endl;
			if(finder!=std::string::npos && ender!=std::string::npos)
				Photo_url.push_back(data.substr(finder,ender-finder));
		}
	}
	
	//if there are urls
	if(Photo_url.size())
	{
		//make output dir
		namespace fs = std::experimental::filesystem;
		fs::create_directory("flickr");
		
		//output list of urls for this run
		//get current time to use as filename
		time_t rawtime;
		struct tm * timeinfo;
		char buffer[80];
	
		time (&rawtime);
		timeinfo = localtime(&rawtime);
	
		strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
		
		std::string str("output.txt");
		//str += ".txt";
		FILE* outlist = fopen(str.c_str(), "wb");
		
		//go through every url in vector
		for(int i = 0; i < Photo_url.size();i++)
		{
			if (!download_jpeg(Photo_url[i].c_str(),PhotoID[i]))
			{
					printf("!! Failed to download file: %s\n", Photo_url[i].c_str());
			}
			else if(outlist)
				fprintf(outlist,"%s\n",Photo_url[i].c_str());
		}
		
		std::string dir = fs::current_path() /+ "flickr";
		GrpImage obj(dir);
		
#if print_dir
		for (auto i : obj.GetList())
			std::cout << i << std::endl;
		std::cout << obj.GetList().size() << std::endl;
#else
		std::string print = obj.Print_stats();
		if(outlist)
			fprintf(outlist,"Output:\n%s\n",print.c_str());
#endif
		
		if(outlist)
			fclose(outlist);
		//delete flickr dir once done
		fs::remove_all("flickr");
	}

	return 0;
}

