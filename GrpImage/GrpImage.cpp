#include "GrpImage.h"

/*
Function Name: read_directory
Parameters: name - directory to look at
			v - vector containing names of all the .jpg files in the directory

*/
void GrpImage::read_directory(const std::string& name, std::vector<std::string>& v)
{
	std::regex Jpg_Regex("((.*)\\.jpg)");

#ifdef _MSC_VER //using windows library to list files in directory
	std::string pattern(name);
	pattern.append("\\*");
	std::string dir(name);
	dir.append("\\");
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
		do {
			//if (std::regex_match(data.cFileName, Jpg_Regex))//check if its a jpg file
				v.push_back(dir + data.cFileName); //push all jpg file directory into a vector
		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
#else //attempt to make cross platform (Doesnt work)
	namespace fs = std::experimental::filesystem;
	for (const auto & entry : fs::directory_iterator(name))
		v.push_back(entry.path());
#endif
}

void GrpImage::img_stats()
{
	for (auto dir : dir_list) //using TinyEXIF to obtain the data from each jpg file
	{
		EXIFStreamFile stream(dir.c_str());
		if (stream.IsValid())
		{
			TinyEXIF::EXIFInfo imageEXIF(stream);
			if (imageEXIF.Fields) 
			{
				//pushing the data into a map
				if (imageEXIF.XResolution || imageEXIF.YResolution || imageEXIF.ResolutionUnit)
				{
					auto Xreso = std::to_string(imageEXIF.XResolution);
					auto Yreso = std::to_string(imageEXIF.YResolution);
					double reso = imageEXIF.XResolution * imageEXIF.YResolution;
					Reso_map[reso] = Xreso + "x" + Yreso;
				}

				if (!imageEXIF.Make.empty())
					++Brand_map[imageEXIF.Make];

				if (!imageEXIF.Model.empty())
					++Model_map[imageEXIF.Model];
			}
			//categorizing the focal lengths
			double Focal = imageEXIF.FocalLength;
			if (Focal >= 14 && Focal <= 35)
				focal_arr[0]++; //wide-angle
			else if (Focal >= 50 && Focal <= 60)
				focal_arr[1]++;//standard
			else if (Focal >= 70 && Focal <= 200)
				focal_arr[2]++;//telephoto
			else
				focal_arr[3]++;//no category
		}
	}
}

GrpImage::GrpImage(std::string str)
	:dir{ str }, dir_list{}, focal_arr{}
{
	read_directory(dir, dir_list);
	img_stats();
}

std::vector<std::string> GrpImage::GetList()
{
	return dir_list;
}

std::string GrpImage::Print_stats()
{
	auto fn = [](auto & p1, auto & p2) {return p1.second < p2.second; };

	std::string printout = "Number of Images: " + std::to_string(dir_list.size()) + "\n";

	if (!Reso_map.empty())
	{
		printout += "Largest Resolution: " + Reso_map.rbegin()->second + "\n";
		printout += "Smallest Resolution: " + Reso_map.begin()->second + "\n";
	}
	else 
	{
		printout += "Largest Resolution: None\n";
		printout += "Smallest Resolution: None\n";
	}

	if (!Brand_map.empty())
		printout += "Most popular Brand: " + std::max_element(Brand_map.begin(), Brand_map.end(), fn)->first + "\n";
	else
		printout += "Most popular Brand: None\n";

	if (!Model_map.empty())
		printout += "Most popular Model: " + std::max_element(Model_map.begin(), Model_map.end(), fn)->first + "\n";
	else
		printout += "Most popular Model: None\n";

	printout += "Number of Wide focal length: " + std::to_string(focal_arr[0]) + "\n";
	printout += "Number of Standard focal length: " + std::to_string(focal_arr[1]) + "\n";
	printout += "Number of Telephoto focal length: " + std::to_string(focal_arr[2]) + "\n";
	printout += "Number of Uncategorized focal length: " + std::to_string(focal_arr[3]) + "\n";
	
	std::cout<< printout<<std::endl;
	
	return printout;
}

